from .action import Action2
from mud.events import ReparerEvent

class ReparerAction(Action2):
    EVENT = ReparerEvent
    ACTION = "reparer"
    RESOLVE_OBJECT = "resolve_for_operate"