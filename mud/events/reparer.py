from .event import Event2

class ReparerEvent(Event2):
    NAME = "reparer"

    def perform(self):
        if not(self.object.has_prop("reparable")):
            self.fail()
            return self.inform("reparer.failed")
        self.inform("reparer")