from .event import Event3

class DechiffrerEvent(Event3):
    NAME = "dechiffrer"

    def perform(self):
        if not(self.object.has_prop("dechiffrable") and self.object2.has_prop("dechiffreur")):
            self.fail()
            return self.inform("dechiffrer.failed")
        self.inform("dechiffrer")